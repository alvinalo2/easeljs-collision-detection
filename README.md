Detectachoques, no es un nombre nada genial, pero la libreria es genial.
consiste de 3 librerias
detectachoquesbitmap, detecta colisiones de objetos que sean de la clase bitmap de EaselJS
detectachoquesprite detecha colisiones de Sprites de EaselJS
detectachoquesvector detecta choques de la clase Graphics de EaselJS, la cual sirve para hacer dibujos vectoriales.
Uso:
este soporta 2 tipo de detectores,
1 
Detecta si los rectángulos imaginarios que rodean estos objetos se superponen, esta es muy imprecisa, pero muy optimizada. Así se usa:
var intersection = ndgmr.checkRectCollision(obj1,obj2);

2
Detecta colisiones Pixel por Pixel, es muy poderoso, pero exige muchos calculos, lo cual lo hace pesado
var collision = ndgmr.checkPixelCollision(obj1,obj2,alphaThreshold,true);

alphaThreshold es una variable que permite omitir objetos con valores de transparencia menores al determinado, el valor por defecto de este es 0
el úlitimo parametro es para obtener una linea con todo el camino antes de la colision, si le damos false, solo da una de 1*1 "un punto".

ejemplo:
var collision = ndgmr.checkPixelCollision(poland,space,0.4,true);

aca detectamos si los bitmaps "poland" y "space" colisionan, le decimos que si uno de estos tiene una opacidad de 0.4 o menos, no lo trate y true es para obtener la ruta de ambos objetos antes del impacto

EN el uso de todas las librerias lo único que cambia es los elementos soportados
detectachoquebitmap = detector de colisiones de mapas de bits "imágenes",
detectachoquesprite = detector de colisiones de sprites,
detectachoquesvector = detectos de colisiones de vectores "de EaselJS",
